package cn.ydxiaoshuai.common;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ReptileBean {
    private int status;
    private String msg;
    private Data data;
    @lombok.Data
    @ToString
    public static class Data{
        private String url;//分析后生成的临时图片地址
        private String sign;//签名值
    }

}
