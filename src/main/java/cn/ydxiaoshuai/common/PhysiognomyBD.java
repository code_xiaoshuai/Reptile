package cn.ydxiaoshuai.common;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 序列化后的对象 大致21个
 */
@Data
@ToString
public class PhysiognomyBD {
    private FaceResult faceResult;
    private FaceShare faceShare;
    private String promotionName;
    private String shareToken;
    private List<DataList> dataList;
    private Activity activity;
    private String homeUrl;
    private String isNaView;
    private String fromWx;
    private String uploadUrl;
    private String shareMode;
    private TiebaShare tiebaShare;
    @Data
    @ToString
    public static class FaceResult{
        private String userImage;
        private List<List<Double>> originDetect;
        private FaceTop faceTop;
        private FaceMark faceMark;
        private List<FaceFate> faceFate;
        private List<String> faceAbstract;
        private List<FaceOrgan> faceOrgan;
        private FaceFive faceFive;
        private FaceFortune faceFortune;
        private int showTips;
    }
    @Data
    @ToString
    public static class FaceTop{
        private Eye eye;
        private Brow brow;
        private Mouth mouth;
        private Nose nose;
        private Shape shape;
    }
    @Data
    @ToString
    public static class Eye{
        private List<Double> pos;
        private String value;
        private String desc;
    }
    @Data
    @ToString
    public static class Brow{
        private List<Double> pos;
        private String value;
        private String desc;
    }
    @Data
    @ToString
    public static class Mouth{
        private List<Double> pos;
        private String value;
        private String desc;
    }
    @Data
    @ToString
    public static class Nose{
        private List<Double> pos;
        private String value;
        private String desc;
    }
    @Data
    @ToString
    public static class Shape{
        private List<Double> pos;
        private String value;
        private String desc;
    }
    @Data
    @ToString
    public static class Organ{
        private String value;
        private String desc;
    }
    @Data
    @ToString
    public static class FaceMark{
        private int faceScore;
        private List<Integer> barChart;
    }
    @Data
    @ToString
    public static class FaceOrgan{
        private String name;
        private List<String> tag;
        private List<Desc> desc;
        private List<String> keyWord;
        private Location location;
        private String image;
    }
    @Data
    @ToString
    public static class Desc{
        private String tag;
        private String desc;
    }
    @Data
    @ToString
    public static class Location{
        private double left;
        private double top;
        private double width;
        private double height;
    }
    @Data
    @ToString
    public static class FaceFive{
        private String faceFiveFeature;
        private FaceFiveMost faceFiveMost;
        private FaceFiveLess faceFiveLess;
        private String faceFiveExplain;
    }
    @Data
    @ToString
    public static class FaceFiveMost{
        private String name;
        private String score;
    }
    @Data
    @ToString
    public static class FaceFiveLess{
        private String name;
        private String score;
    }
    @Data
    @ToString
    public static class FaceFortune{
        private List<Integer> careerScores;
        private List<Integer> healthScores;
        private List<Integer> moneyScores;
        private List<Integer> loveScores;
        private List<String> fortuneExplain;
        private int showAll;
    }
    @Data
    @ToString
    public static class FaceFate{
        private String name;
        private double score;
        private String text;
    }
    @Data
    @ToString
    public static class FaceShare{
        private int happyLot;
        private List<String> motto;
        private List<Organ> organ;
        private String userImage;
        private String qrcode;
        private String shareUrl;
    }
    @Data
    @ToString
    public static class DataList{
        private int toolboxImg;
        private String toolboxText;
        private String toolboxUrl;
    }
    @Data
    @ToString
    public static class Activity{
        private int showActivityHint;
        private int activityState;
    }
    @Data
    @ToString
    public static class TiebaShare{
        private String appKey;
        private String appName;
        private String appName_en;
        private String avatar;
        private String picture;
        private String path;
    }
}
